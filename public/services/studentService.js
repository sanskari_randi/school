angular.module('studentService', [])
    .factory('studentService', function($http){
        var studentFactory = {};

        //get single student
        studentFactory.get = function (id) {
            return $http.get('/api/students/' +id);
        };

        //get all students
        studentFactory.all = function () {
            return $http.get('/api/students/');
        };

        //create student
        studentFactory.create = function (studentData) {
            return $http.post('/api/students' , studentData);
        };

        //update student
        studentFactory.update = function (id, studentData) {
            return $http.put('/api/students'+ id, studentData);
        };

        //delete student
        studentFactory.delete = function (id) {
            return $http.delete('/api/students/' + id);
        };

        return studentFactory;

    });