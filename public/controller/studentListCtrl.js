angular.module('studentListCtrl', [])
    .controller('studentListCtrl', studentList);

    function studentList(studentService) {
        var vm = this;

        //vm.message = 'this is all student page';

        studentService.all().success(function (data) {
            vm.students = data;
            //console.log(students._id);
        });

        vm.deleteStudent = function(studentId) {
            studentService.delete(studentId).success(function () {
                console.log('Student Deleted');
            });

        };


      
    };