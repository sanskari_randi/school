angular.module('addStudentCtrl', [])
    .controller('addStudentCtrl', addStudent);

    function addStudent(studentService){
        var vm = this;
        vm.message = "this page is for Adding new students";

        //created a empty object to hold the scope data of form
        vm.holderForFormData = {};

        
        vm.submitForm = function () {
            studentService.create(vm.holderForFormData)  // this vm will get the data from the scope and save it to the holder

                .success(function () {
                console.log('User added Successfully');
            });
        }
}